﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Documentation;
using System.IO;

namespace Documentation.Test
{
    [TestClass]
    public class FileUtilsTest
    {
        private String _destinationDirectory;

        [TestInitialize]
        public void SetUp()
        {
            _destinationDirectory = "D:/documenation/";
            if (Directory.Exists(_destinationDirectory))
            {
                string[] files = Directory.GetFiles(_destinationDirectory);
                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }
                Directory.Delete(_destinationDirectory);
            }
        }

        [TestMethod]
        public void ShouldCreateDirectory()
        {
            FileUtils.CreateDirectory(_destinationDirectory);
            Assert.IsTrue(Directory.Exists(_destinationDirectory));
        }

        [TestCleanup]
        public void CleanUp()
        {
            string[] files = Directory.GetFiles(_destinationDirectory);
            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }
            Directory.Delete(_destinationDirectory);
        }
    }
}
