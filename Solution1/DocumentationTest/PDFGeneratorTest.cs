﻿using System;
using NMock2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Documentation;
using System.IO;
using System.Collections.Generic;
using DataAccessLayer;


namespace Documentation.Test
{
    [TestClass]
    public class PDFGeneratorTest
    {
        private PDFGenerator _pdfGenerator;
        private String _destinationDirectory;
        private String _fileName;
        private String _defaultFileName;
        private Mockery mocks;
        private DatabaseManager mockDatabaseManager;
        List<Result> names = new List<Result>();
        List<Result> info = new List<Result>();
        List<Result> info1 = new List<Result>();


        [TestInitialize]
        public void Setup()
        {    
            

            mocks = new Mockery();

            mockDatabaseManager = mocks.NewMock<DatabaseManager>(new DatabaseConnection("baza",true,true));

            _pdfGenerator = new PDFGenerator(mockDatabaseManager);
            _destinationDirectory = "D:/documentation/";
            _fileName = "documentation1.pdf";
            _defaultFileName = "documentation.pdf";

            if (File.Exists(_destinationDirectory + _fileName))
                File.Delete(_destinationDirectory + _fileName);

            if (File.Exists(_destinationDirectory + _defaultFileName))
                File.Delete(_destinationDirectory + _defaultFileName);

            
            Result res1 = new Result();
            Result res2 = new Result();
            Result res3 = new Result();
            Result res4 = new Result();
            Result res5 = new Result();
            res1.Put("name", "tabela");
            res2.Put("name", "ID");
            res2.Put("type", "integer");
            res3.Put("name", "tabela1");
            res4.Put("name", "number");
            res4.Put("type", "double");
            res5.Put("name", "Surname");
            res5.Put("type", "character");
            names.Add(res1);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res1);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            names.Add(res3);
            info.Add(res2);
            info1.Add(res4);
            info1.Add(res5);


            
        }

        [TestMethod]
        public void ShouldReturnTrueForSuccesfulDocumentationGeneration()
        {

            Stub.On(mockDatabaseManager).Method("GetTablesNames").Will(Return.Value(names));

            Stub.On(mockDatabaseManager).Method("GetTableInfo").With("tabela").Will(Return.Value(info));
            Stub.On(mockDatabaseManager).Method("GetTableInfo").With("tabela1").Will(Return.Value(info1)); 
            Assert.IsTrue(_pdfGenerator.GenerateDocumentation(_destinationDirectory));
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void ShouldStoreGeneratedFile()
        {
            Stub.On(mockDatabaseManager).Method("GetTablesNames").Will(Return.Value(names));
            Stub.On(mockDatabaseManager).Method("GetTableInfo").With("tabela").Will(Return.Value(info));
            Stub.On(mockDatabaseManager).Method("GetTableInfo").With("tabela1").Will(Return.Value(info1));

            _pdfGenerator.GenerateDocumentation(_destinationDirectory);
            _pdfGenerator.GenerateDocumentation(_destinationDirectory, _fileName);
            Assert.IsTrue(File.Exists(_destinationDirectory + _fileName));
            Assert.IsTrue(File.Exists(_destinationDirectory + _defaultFileName));

            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestCleanup]
        public void CleanUp()
        {
            //File.Delete(_destinationDirectory + _fileName);
            //File.Delete(_destinationDirectory + _defaultFileName);
        }    
    }
}
