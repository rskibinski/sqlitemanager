﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using System.Collections.Generic;
using NMock2;

namespace DataAccessLayerTest
{
    [TestClass]
    public class DatabaseConnectionManagerTest
    {
        private Mockery mocks;
        private DatabaseConnection dbConnMock;

        [TestMethod]
        public void ShouldCreateTable()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            Expect.Once.On(dbConnMock).Method("Execute").With("CREATE TABLE Table1 (x INTEGER, y INTEGER)").Will(Return.Value(1));
            Expect.Once.On(dbConnMock).Method("Execute").With("CREATE TABLE Table2 (x INTEGER, y REAL)").Will(Return.Value(1));
            manager.CreateTable("Table1", new Dictionary<String, String>()
            {
                {"x","INTEGER"},
                {"y","INTEGER"}
            });
            manager.CreateTable("Table2", new Dictionary<String, String>()
            {
                {"x","INTEGER"},
                {"y","REAL"}
            });
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void ShouldDropTable()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            Expect.Once.On(dbConnMock).Method("Execute").With("DROP TABLE Table1").Will(Return.Value(1));
            manager.DropTable("Table1");
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void ShouldExecuteQuery()
        {
            //todo
        }

        [TestMethod]
        public void ShouldGetTable()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            List<Result> resultStub = new List<Result>();
            Expect.Once.On(dbConnMock).Method("Select").With("SELECT * FROM Table1").Will(Return.Value(resultStub));
            Assert.AreSame(manager.GetTable("Table1"),resultStub);
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void ShouldGetTableInfo()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            List<Result> resultStub = new List<Result>();
            Expect.Once.On(dbConnMock).Method("Select").With("PRAGMA table_info(Table1)").Will(Return.Value(resultStub));
            Assert.AreSame(manager.GetTableInfo("Table1"), resultStub);
            mocks.VerifyAllExpectationsHaveBeenMet();
        }


        [TestMethod]
        public void ShouldGetTablesNames()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            List<Result> resultStub = new List<Result>();
            Expect.Once.On(dbConnMock).Method("Select").With("SELECT * FROM sqlite_master WHERE type IN ('table')").Will(Return.Value(resultStub));
            Assert.AreSame(manager.GetTablesNames(), resultStub);
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void ShouldInsertIntoTable()
        {
            mocks = new Mockery();
            dbConnMock = mocks.NewMock<DatabaseConnection>("irrelevant", true, true);
            DatabaseManager manager = new DatabaseManager(dbConnMock);
            String tableName = "someTable";
            Dictionary<String,Object> columnValues = new Dictionary<String,Object>() { 
                {"A", 1234},
                {"B", "Value1"}
            };
            String expectedString = "INSERT INTO "+tableName+" (A,B) VALUES (1234,'Value1')";
            Expect.Once.On(dbConnMock).Method("Execute").With(expectedString).Will(Return.Value(1));
            Assert.IsTrue(manager.InsertIntoTable(tableName, columnValues) == 1);
            mocks.VerifyAllExpectationsHaveBeenMet();
        }

    }
}
