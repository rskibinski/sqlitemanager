﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SQLite;
using DataAccessLayer;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayerTest
{
    [TestClass]
    public class DatabaseConnectionTest
    {

        [TestMethod]
        public void shouldOpenConnection()
        {
            DatabaseConnection connection = new DatabaseConnection("irrelevant", true, true);
            connection.Open();
            Assert.AreEqual(connection.State, ConnectionState.Open);
        }

        [TestMethod]
        public void shouldCloseConnection()
        {
            DatabaseConnection connection = new DatabaseConnection("irrelevant", true, true);
            connection.Open();
            connection.Dispose();
            Assert.AreEqual(connection.State, ConnectionState.Closed);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void shouldThrowExceptionWhenPerformingExecutionOnClosedConnection()
        {
            DatabaseConnection connection = new DatabaseConnection("irrelevant", true, true);
            connection.Execute("SOME IRRELEVANT QUERY");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Already opened")]
        public void shouldThrowExceptionWhenOpeningMultipleTimes()
        {
            DatabaseConnection connection = new DatabaseConnection("irrelevant", true, true);
            connection.Open();
            connection.Open();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Already opened")]
        public void shouldThrowExceptionWhenClosingMultipleTimes()
        {
            DatabaseConnection connection = new DatabaseConnection("irrelevant", true, true);
            connection.Open();
            connection.Dispose();
            connection.Dispose();
        }

        [TestMethod]
        public void shouldExecuteCreateTableStatement()
        {
            using (DatabaseConnection connection = new DatabaseConnection("somedb", true, true))
            {
                connection.Open();
                const string SQL_QUERY = "CREATE TABLE SomeTable (x INTEGER, y INTEGER, z INTEGER)";
                Assert.IsTrue(connection.Execute(SQL_QUERY) == 0);
                const string SQL_CHECK_IF_TABLE_CREATED = "SELECT count(*) FROM SomeTable";
                Assert.IsNotNull(connection.Execute(SQL_CHECK_IF_TABLE_CREATED));
            }


        }

        [TestMethod]
        public void shouldExecuteInsertStatement()
        {
            using (DatabaseConnection connection = new DatabaseConnection("somedb", true, true))
            {
                connection.Open();
                const string SQL_CREATE_TABLE = "CREATE TABLE SomeTable (x INTEGER, y INTEGER, z INTEGER)";
                const string SQL_INSERT = "INSERT INTO SomeTable VALUES (1, 2, 3)";
                connection.Execute(SQL_CREATE_TABLE);
                Assert.AreEqual(connection.Execute(SQL_INSERT), 1);
            }
            
        }

        [TestMethod]
        public void shouldExecuteSelectStatement()
        {
            using (DatabaseConnection connection = new DatabaseConnection("somedb", true, true))
            {
                connection.Open();
                const string SQL_CREATE_TABLE = "CREATE TABLE SomeTable (x INTEGER, y REAL, z INTEGER)";
                const string SQL_INSERT = "INSERT INTO SomeTable VALUES (1, 2.01, 3)";
                const string SQL_INSERT1 = "INSERT INTO SomeTable VALUES (4, 5.02, 6)";
                const string SQL_SELECT = "SELECT * FROM SomeTable";
                connection.Execute(SQL_CREATE_TABLE);
                connection.Execute(SQL_INSERT);
                connection.Execute(SQL_INSERT1);
                List<Result> result = connection.Select(SQL_SELECT);
                Assert.IsNotNull(result);
                Assert.AreEqual(result.Count, 2);
                Assert.AreEqual(1, result[0].GetInteger("x"));
                Assert.AreEqual(4, result[1].GetInteger("x"));
                Assert.AreEqual(2.01, result[0].GetDouble("y"));
            }  
        }


    }
}
