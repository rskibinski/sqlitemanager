﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentation
{
    public static class FileUtils
    {
        public static bool CreateDirectory(String path)
        {
            System.IO.Directory.CreateDirectory(path);
            return true;
        }
    }
}
