﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentation
{
    interface IPDFGenerator
    {
        bool GenerateDocumentation(String destinationDirectoryPath, String fileName = "documentation.pdf");
    }
}
