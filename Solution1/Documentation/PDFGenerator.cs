﻿using System;
using System.Collections.Generic;
using PdfSharp.Drawing;

using PdfSharp.Pdf;
using DataAccessLayer;

namespace Documentation
{
    /// <summary>
    /// Class, that provides PDF database documentation generation functionality.
    /// </summary>
    public class PDFGenerator : IPDFGenerator
    {
        private DatabaseManager manager;
        public PDFGenerator(DatabaseManager manager) 
        {
            this.manager = manager;
        }

        /// <summary>
        /// This method generates PDF documentation and stores one in provided directory. File name: according to the fileName parameter.
        /// </summary>
        /// <param name="destinationDirectoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool GenerateDocumentation(String destinationDirectoryPath, String fileName = "documentation.pdf")
        {
            if (!System.IO.Directory.Exists(destinationDirectoryPath))
            {
                FileUtils.CreateDirectory(destinationDirectoryPath);
            }

            List<Result> table_names = manager.GetTablesNames();

            PdfDocument document = new PdfDocument();
            document.Info.Title = "Created with PDFsharp";
            PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
            XFont font1 = new XFont("Verdana", 12, XFontStyle.BoldItalic);
            List<Result> table_info = new List<Result>();
            int tmptext = 1;
            int page_count = 0;
            int page_len = 0;
            double width = 0;
            double max_width = 0;
            int start, end;

            foreach (Result tablename in table_names)
            {

                table_info = manager.GetTableInfo(tablename.GetString("name"));
                width = gfx.MeasureString(tablename.GetString("name"), font).Width;
                max_width = width;
                foreach (Result column in table_info)
                {
                    width = gfx.MeasureString(column.GetString("name") + ": " + column.GetString("type"), font1).Width;
                    if (max_width < width) max_width = width;
                }

                
                if (page_len > 700)
                {
                    page = document.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    page_count++;
                    tmptext = 1;
                    page_len = 0;
                }
                
                start = tmptext;

                gfx.DrawLine(XPens.Black, 1, tmptext, max_width+3, tmptext);
                tmptext += 1;
                page_len += 1;
                
                gfx.DrawString(tablename.GetString("name"), font, XBrushes.Black,
                new XRect(3, tmptext, page.Width, page.Height),
                XStringFormats.TopLeft);

                //width = gfx.MeasureString(tablename.GetString("name"), font).Width;
                //gfx.DrawLine(XPens.Black, 1, tmpline, max_width, tmpline);

                tmptext += 25;
                page_len += 25;
                foreach (Result column in table_info)
                {
                    
                    gfx.DrawLine(XPens.Black, 1, tmptext, max_width+3, tmptext);
                    tmptext += 1;
                    page_len += 1;
                    
                    gfx.DrawString(column.GetString("name") + ": " + column.GetString("type"), font1, XBrushes.Black,
                    new XRect(3,tmptext, page.Width, page.Height),
                    XStringFormats.TopLeft);
                    //width = gfx.MeasureString(tablename.GetString("name"), font).Width;
                    tmptext += 16;
                    page_len += 16;
                    //gfx.DrawLine(XPens.Black, 1, tmpline, width, tmpline);
                }
                
                gfx.DrawLine(XPens.Black, 1, tmptext, max_width+3, tmptext);
                end = tmptext;
                gfx.DrawLine(XPens.Black, 1, start, 1, end);
                gfx.DrawLine(XPens.Black, max_width+3, start, max_width+3, end);
                tmptext += 30;
                page_len += 30;
            }

            string filename = destinationDirectoryPath + fileName;
            document.Save(filename);
            return true;
        }
    }
}
