﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DatabaseManager
    {
        private IDatabaseConnection connection;
        public DatabaseManager(IDatabaseConnection connection)
        {
            this.connection = connection;
        }

        /// <summary>
        /// Returns a list representing each column in a database. Ask for column name/type by
        /// result[columnIndex].getString("type"/"name"); More info is included, check SQLite PRAGMA table_info for information.
        /// </summary>
        /// <param name="table">Table whick info is to be fetched</param>
        /// <returns>List of columns</returns>
        public virtual List<Result> GetTableInfo(String table)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("PRAGMA table_info(");
            sb.Append(table);
            sb.Append(")");
            using (connection)
            {
                connection.Open();
                return connection.Select(sb.ToString());
            }
        }

        /// <summary>
        /// Returns a list representing each table in a database
        /// Ask for table name by result[tableindex].getString("name")
        /// </summary>
        /// <returns>List of tables</returns>
        public virtual List<Result> GetTablesNames()
        {
            string sql = "SELECT * FROM sqlite_master WHERE type IN ('table')";
            using (connection)
            {
                connection.Open();
                return connection.Select(sql);
            }
        }

        /// <summary>
        /// Returns a select * statement for a given table
        /// </summary>
        /// <param name="table">Table name</param>    
        /// <returns>List containing each row of table</returns>
        public List<Result> GetTable(String table)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM ");
            sb.Append(table);
            using (connection)
            {
                connection.Open();
                return connection.Select(sb.ToString());
            }
        }

        /// <summary>
        /// Creates a table with given table name and given columns
        /// </summary>
        /// <param name="table">string with table name</param>
        /// <param name="cols">column names to types mapping</param>
        public void CreateTable(String table, Dictionary<string, string> cols)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("CREATE TABLE ");
            sb.Append(table);
            sb.Append(" (");
            foreach (KeyValuePair<string,string> pair in cols)
            {
                sb.Append(pair.Key);
                sb.Append(" ");
                sb.Append(pair.Value);
                sb.Append(", ");
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append(")");

            using (connection) 
            {
                connection.Open();
                connection.Execute(sb.ToString());
            }
        }

        /// <summary>
        /// Drops a given table
        /// </summary>
        /// <param name="table">table name</param>
        public void DropTable(String table)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("DROP TABLE ");
            sb.Append(table);
            using (connection)
            {
                connection.Open();
                connection.Execute(sb.ToString());
            }
        }


        /// <summary>
        /// Inserts one row into specified table, with given values for specified columns
        /// </summary>
        /// <param name="tableName"></param>        table name on which insert in to be performed
        /// <param name="colsWithValues"></param>   cols to values mapping as a dictionary
        public int InsertIntoTable(String tableName, Dictionary<String,Object> colsWithValues)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder valuesBuilder = new StringBuilder();
            sb.Append("INSERT INTO ");
            sb.Append(tableName);
            sb.Append(" (");
            valuesBuilder.Append("VALUES (");
            foreach (String column in colsWithValues.Keys) {
                sb.Append(column);
                sb.Append(",");
                if (colsWithValues[column] is string)  
                {
                    valuesBuilder.Append("'");
                    valuesBuilder.Append(colsWithValues[column]);
                    valuesBuilder.Append("'");
                }
                else
                {
                    valuesBuilder.Append(colsWithValues[column]);
                }

                valuesBuilder.Append(",");
            }
            sb.Remove(sb.Length-1, 1);
            valuesBuilder.Remove(valuesBuilder.Length - 1, 1);
            sb.Append(") ");
            valuesBuilder.Append(")");
            sb.Append(valuesBuilder.ToString());
            using (connection)
            {
                connection.Open();
                return connection.Execute(sb.ToString());
            }
        }

        /// <summary>
        /// Executes raw query
        /// </summary>
        /// <param name="query">query to be executed</param>
        public void ExecuteQuery(String query)
        {
            //todo
        }

    }
}
