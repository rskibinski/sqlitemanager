﻿using System;
namespace DataAccessLayer
{
    public interface IDatabaseConnection : IDisposable
    {
        int Execute(string sql);
        void Open();
        System.Collections.Generic.List<Result> Select(string sql);
        System.Data.ConnectionState State { get; }
    }
}
