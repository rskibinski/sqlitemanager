﻿using System;
namespace DataAccessLayer
{
    public interface IDatabaseManager
    {
        void CreateTable(string table, System.Collections.Generic.Dictionary<string, string> cols);
        void DropTable(string table);
        void ExecuteQuery(string query);
        System.Collections.Generic.List<Result> GetTable(string table);
        System.Collections.Generic.List<Result> GetTableInfo(string table);
        System.Collections.Generic.List<Result> GetTablesNames();
        int InsertIntoTable(string tableName, System.Collections.Generic.Dictionary<string, object> colsWithValues);
    }
}
