﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace DataAccessLayer
{
    public class DatabaseConnection : IDisposable, DataAccessLayer.IDatabaseConnection
    {
        private string databaseName;
        private bool inMemory;
        private bool createNew;
        private string connectionString;

        private SQLiteConnection connection;
        public ConnectionState State
        {
            get
            {
                return connection == null ? ConnectionState.Closed : connection.State;
            }
        }

        /// <summary>
        /// Creates a new DatabaseConnection object 
        /// </summary>
        /// <param name="dataBaseName">Name of the database file (if in memory is false)</param>
        /// <param name="inMemory">If database is to be kept in memory</param>
        /// <param name="createNew">Overwrites old file</param>
        public DatabaseConnection(string dataBaseName, bool inMemory, bool createNew)
        {
            this.databaseName = dataBaseName;
            this.inMemory = inMemory;
            this.createNew = createNew;
            this.connectionString = CreateConnectionString();
        }


        /// <summary>
        /// Opens a new connection to the database
        /// </summary>
        public void Open()
        {
            if (CheckConnection()) throw new InvalidOperationException("Already opened");
            else
            {
                connection = new SQLiteConnection(connectionString);
                connection.Open();
            }
        }

        /// <summary>
        /// Closes connection, used in using {...} block
        /// </summary>
        public void Dispose()
        {
            if (CheckConnection()) connection.Close();
            else throw new InvalidOperationException("Already closed");
        }


        /// <summary>
        /// Executes a statement which alters the underlying database
        /// </summary>
        /// <param name="sql">sql statement</param>
        /// <returns>Number of rows affected</returns>
        public virtual int Execute(string sql)
        {
            if (!CheckConnection()) ThrowExceptionWhenNotOpened();
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = sql;
            return command.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes a select query on underlying database
        /// </summary>
        /// <param name="sql">query to be performed</param>
        /// <returns>List of results</returns>
        public virtual List<Result> Select(string sql)
        {
            if (!CheckConnection()) ThrowExceptionWhenNotOpened();
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = sql;
            SQLiteDataReader reader = command.ExecuteReader();


            List<Result> results = new List<Result>();
            while (reader.Read())
            {
                Result newResult = new Result();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    newResult.Put(reader.GetName(i), reader.GetValue(i));
                }
                results.Add(newResult);
            }
            return results;
        }

        /// <summary>
        /// Creates a connectino string from connection parameters
        /// </summary>
        /// <returns></returns>
        private string CreateConnectionString()
        {
            StringBuilder sb = new StringBuilder();
            if (inMemory)
            {
                sb.Append("Data Source=:memory:;");
            }
            else
            {
                sb.Append("Data Source=");
                sb.Append(databaseName);
                sb.Append(";");
            }
            if (createNew)
            {
                sb.Append("New=True;");
            }
            else
            {
                sb.Append("New=False;");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Checks if connection is open
        /// </summary>
        /// <returns></returns>
        private bool CheckConnection()
        {
            return connection != null && connection.State == ConnectionState.Open ? true : false;
        }

        private void ThrowExceptionWhenNotOpened()
        {
            throw new InvalidOperationException("Connection not opened");
        }

    }

}
