﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Result
    {
        private Dictionary<String, Object> _result = new Dictionary<string,object>();


        /*begin just for testing - to delete - sorry for trouble */
        public Result()
        {
        }

        public Result(string name, object object1 = null)
        {

            _result.Add(name, object1);
        }
        /* end just for testing - to delete - sorry for trouble */
        public string GetString(String columnName)
        {
            return _result[columnName].ToString();
        }

        public double GetDouble(String columnName)
        {
            return Double.Parse(_result[columnName].ToString());
        }

        public int GetInteger(String columnName)
        {
            return int.Parse(_result[columnName].ToString());
        }

        public void Put(String columnName, Object value)
        {
            _result.Add(columnName, value);
        }
    }
}
